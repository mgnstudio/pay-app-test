import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { CreateComponent } from './../components/create/create.component';
import { FormComponent } from './../components/form/form.component';
import { HistoryComponent } from './../components/history/history.component';
import { LayoutComponent } from './../components/layout/layout.component';

import { OnlyNumbersDirective } from './../directives/only-numbers.directive';

import { CardMaskPipe } from './../pipes/card.pipe';
import { RuCurrencyMaskPipe } from './../pipes/ru-currency.pipe';

import { TextMaskModule } from 'angular2-text-mask';

const appRoutes: Routes = [
  {
    path: '',
    component: LayoutComponent,
    children: [
      { path: 'create', component: CreateComponent },
      { path: 'history', component: HistoryComponent },
      { path: '', redirectTo: '/create', pathMatch: 'full' }
    ]
  }
];

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    TextMaskModule,
    RouterModule.forRoot(appRoutes)
  ],
  declarations: [
    AppComponent,
    CreateComponent,
    HistoryComponent,
    LayoutComponent,
    CardMaskPipe,
    RuCurrencyMaskPipe,
    FormComponent,
    OnlyNumbersDirective
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
