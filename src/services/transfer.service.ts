import { Injectable } from '@angular/core';
import { Transfer } from './../model/transfer.model';

@Injectable({
  providedIn: 'root'
})
export class TransferService {
  constructor() {}

  /**
   * items {get set}
   */
  private itemsCache: Transfer[];
  public get items() {
    if (!this.itemsCache) {
      this.itemsCache = (
        JSON.parse(localStorage.getItem('transfers')) || []
      ).map(m => {
        return {
          ...(m as Transfer),
          transferDate: m.transferDate ? new Date(m.transferDate) : null
        };
      });
    }
    return this.itemsCache;
  }
  public set items(value: Transfer[]) {
    localStorage.setItem('transfers', JSON.stringify(value));
    this.itemsCache = value;
  }

  /**
   * Получение одного эл-та
   */
  public getItem(index: number) {
    const item = this.items[index];
    return item;
  }

  /**
   * Новый эл-т из модели формы
   */
  public async add(form: Transfer) {
    this.items = [...this.items, form];
    return this.items;
  }

  /**
   * Удалить эл-т по индексу(id не используем)
   */
  public async remove(index: number) {
    this.items = this.items.filter((f, i) => i !== index);
    return this.items;
  }
}
