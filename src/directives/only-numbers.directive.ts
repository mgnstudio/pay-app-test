import { Directive, Input, HostListener } from '@angular/core';
import { NgControl } from '@angular/forms';

@Directive({
  selector: '[onlyNumbers]'
})
export class OnlyNumbersDirective {
  private el: NgControl;

  @Input() useDecimal: boolean;

  constructor(private ngControl: NgControl) {
    this.el = ngControl;
  }

  // Listen for the input event to also handle copy and paste.
  @HostListener('input', ['$event.target.value'])
  onInput(value: string) {
    console.log('this.useDecimal', this.useDecimal);
    // Use NgControl patchValue to prevent the issue on validation
    this.el.control.patchValue(
      value.replace(this.useDecimal ? /[^0-9.]/g : /[^0-9]/g, '')
    );
  }
}
