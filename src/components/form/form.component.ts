import { Component, Output, OnInit, EventEmitter, Input } from '@angular/core';

import {
  FormGroup,
  FormBuilder,
  Validators,
  AbstractControl
} from '@angular/forms';
import { TransferViewModel } from './../../view-model/transfer.view-model';

@Component({
  selector: 'app-form',
  templateUrl: `form.component.html`,
  styleUrls: ['form.component.scss']
})
export class FormComponent implements OnInit {
  public readonly YEARS_COUNT: number = 5;

  public readonly CARD_MASK = [
    /\d/,
    /\d/,
    /\d/,
    /\d/,
    ' ',
    /\d/,
    /\d/,
    /\d/,
    /\d/,
    ' ',
    /\d/,
    /\d/,
    /\d/,
    /\d/,
    ' ',
    /\d/,
    /\d/,
    /\d/,
    /\d/
  ];
  public readonly CVC_MASK = [/\d/, /\d/, /\d/];

  public form: FormGroup;

  public months: number[];
  public lastDisabledMonth: number;
  public years: number[];

  @Output() submitEvent: EventEmitter<any> = new EventEmitter<any>();
  @Input() model: TransferViewModel = new TransferViewModel();

  constructor(private fb: FormBuilder) {
    this.generateDateOptions();
  }

  /**
   * После инициализации,
   * заполняем форму из модели, для валидации
   */
  ngOnInit() {
    const group = {
      senderNum: [
        this.model.sender.num,
        [Validators.required, this.ValidateCard]
      ],
      senderName: [
        this.model.sender.name,
        [Validators.required, Validators.pattern(/^[A-z0-9 .]*$/)]
      ],
      senderDateMonth: [this.model.sender.date.month, Validators.required],
      senderDateYear: [this.model.sender.date.year, Validators.required],
      senderCvc: ['', [Validators.required, Validators.pattern(/^[\d]{3}$/)]],
      receiverNum: [
        this.model.receiver.num,
        [Validators.required, this.ValidateCard]
      ],
      summ: [
        this.model.summ,
        [Validators.required, Validators.pattern(/^(\d+\.?\d*|\.\d+)$/)]
      ]
    };

    this.form = this.fb.group(group);
  }

  /**
   * Валидация карты
   */
  ValidateCard(control: AbstractControl) {
    // dddd dddd dddd dddd
    // dddddddddddddddd
    if (
      !/^[\d]{4}[ ][\d]{4}[ ][\d]{4}[ ][\d]{4}$/.test(control.value) &&
      !/^[\d]{16}$/.test(control.value)
    ) {
      return { message: 'error' };
    }
    return null;
  }

  /**
   * Генерация справочников, для select
   */
  private generateDateOptions() {
    const currentDate = new Date();
    const currentYear = currentDate.getFullYear();

    this.months = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
    this.years = new Array(this.YEARS_COUNT)
      .fill(undefined)
      .map((_, i) => i + currentYear);
  }

  /**
   * У текущего года, все месяцы что прошли нужно отключить
   */
  public setLastDisableMonth(year?: number) {
    const currentDate = new Date();
    const currentYear = currentDate.getFullYear();
    const currentMonth = currentDate.getMonth();

    this.lastDisabledMonth = currentYear === year ? currentMonth : 0;
  }

  /**
   * Событие поcле обновления year,
   * для сброса month(если нужно)
   */
  yearChange(event) {
    const year = this.form.value.senderDateYear;
    const month = this.form.value.senderDateMonth;

    this.setLastDisableMonth(year);
    this.clearMonth(year, month);
  }

  /**
   * Сбрасываем месяц если он уже прошел
   */
  clearMonth(year, month) {
    const currentDate = new Date();
    const currentYear = currentDate.getFullYear();
    const currentMonth = currentDate.getMonth();

    if (year === currentYear && currentMonth > month) {
      this.form.patchValue({
        senderDateMonth: null
      });
    }
  }

  onSubmit() {
    this.submitEvent.emit(this.model);
  }
}
