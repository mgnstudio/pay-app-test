import { Component, Input } from '@angular/core';
import { TransferService } from './../../services/transfer.service';
import { Transfer } from './../../model/transfer.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-history-component',
  templateUrl: `history.component.html`,
  styleUrls: ['history.component.scss']
})
export class HistoryComponent {
  items: Transfer[];
  constructor(
    private transferService: TransferService,
    private router: Router
  ) {
    this.items = transferService.items;
  }

  remove(i: number) {
    this.transferService.remove(i).then(items => {
      this.items = items;
    });
  }

  navigate(i: number) {
    this.router.navigateByUrl('/create', { state: { index: i } });
  }
}
