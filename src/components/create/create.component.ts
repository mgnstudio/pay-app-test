import { Component, Input, OnInit } from '@angular/core';
import { TransferViewModel } from './../../view-model/transfer.view-model';
import { TransferService } from './../../services/transfer.service';
import { Transfer } from './../../model/transfer.model';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-create-container',
  templateUrl: `create.component.html`,
  styleUrls: ['create.component.scss']
})
export class CreateComponent implements OnInit {
  state$: Observable<any>;
  form: TransferViewModel = new TransferViewModel();

  constructor(
    private transferService: TransferService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {
    this.state$ = this.activatedRoute.paramMap.pipe(() => window.history.state);

    const index = this.state$['index'];

    const item = this.transferService.getItem(index);
    if (item) {
      this.form.fill(item);
    }
  }

  onSend(formModel) {
    const transfer: Transfer = new Transfer();
    transfer.from(formModel);
    transfer.transferDate = new Date();
    this.transferService.add(transfer);

    this.router.navigateByUrl('/history');
  }
}
