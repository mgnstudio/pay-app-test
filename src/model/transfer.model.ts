import { TransferViewModel } from './../view-model/transfer.view-model';

export class Transfer {
  public sender: {
    num: string;
    name: string;
    date: {
      month: number;
      year: number;
    };
  };
  public receiver: {
    num: string;
  };

  public summ: number;
  public transferDate: Date;

  public constructor() {}

  /**
   * Заполнить из формы(под новый эл-т)
   */
  public from(obj: TransferViewModel) {
    this.sender = {
      num: obj.sender.num.replace(/\s/g, ''),
      name: obj.sender.name,
      date: {
        month: obj.sender.date.month,
        year: obj.sender.date.year
      }
    };

    this.receiver = {
      num: obj.receiver.num.replace(/\s/g, '')
    };

    this.summ = obj.summ;
  }
}
