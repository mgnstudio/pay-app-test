import { Transfer } from 'src/model/transfer.model';

export class TransferViewModel {
  /**
   * Отпрвитель
   */
  public sender: {
    num: string;
    name: string;

    // CVC2/CVV2
    code: string;
    date: {
      month: number;
      year: number;
    };
  };

  /**
   * Получатель
   */
  public receiver: {
    num: string;
  };

  /**
   * Сумма перевода
   */
  public summ: number;

  public constructor() {
    this.sender = {
      num: '',
      name: '',
      code: '',
      date: {
        month: null,
        year: null
      }
    };

    this.receiver = {
      num: ''
    };
  }

  /**
   * Заполнить из эл-та в хранилище
   */
  public fill(obj: Transfer) {
    this.sender = {
      num: obj.sender.num,
      name: obj.sender.name,
      code: '',
      date: {
        month: obj.sender.date.month,
        year: obj.sender.date.year
      }
    };

    this.receiver = {
      num: obj.receiver.num
    };

    this.summ = obj.summ;
  }
}
