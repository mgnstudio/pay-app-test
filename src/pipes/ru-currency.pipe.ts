import { PipeTransform, Pipe } from '@angular/core';

// Без использования locale
@Pipe({
  name: 'ruCurrencyMask'
})
export class RuCurrencyMaskPipe implements PipeTransform {
  transform(price: string): string {
    return `${price.replace(/\B(?=(\d{3})+(?!\d))/g, ' ').trim()} руб.`;
  }
}
