import { PipeTransform, Pipe } from '@angular/core';

@Pipe({
  name: 'cardMask'
})
export class CardMaskPipe implements PipeTransform {
  transform(card: string): string {
    return card
      .replace(/\s+/g, '')
      .replace(/(\d{4})/g, '$1 ')
      .replace(/\b(?!^)(\d{4}[ ]\d{4})/gm, `**** ****`)
      .trim();
  }
}
